package digi;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Scanner;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

public class PdfReader
{

    public static Hashtable<String, String> getPDFData(String sPath, String sPDFFilePath) throws IOException
    {

        int iFoundAtLine = 0;
        String sdata = "", s;
        String line = null;
        Hashtable<String, String> PDFData = new Hashtable<String, String>();
        PdfReader.readPDF(sPDFFilePath);
        /**************************************************************
         * Convert PDF to Text file
         **************************************************************/
        readPDF(sPDFFilePath);

        /**************************************************************
         * Find the line number of header
         **************************************************************/
        FileReader fileReader = null;
        // FileReader reads text files in the default encoding.
        fileReader = new FileReader(sPath);
        // Always wrap FileReader in BufferedReader.
        BufferedReader bufferedReader = new BufferedReader(fileReader);

        while ((line = bufferedReader.readLine()) != null)
        {

            switch (line)
            {

                case "Mortgage":// Mortgage Applied For
                    // CheckBox
                    break;
                case "Agency Case Number ":
                    s = searchInfile(sPath, "Agency Case Number", 1);
                    PDFData.put("AgencyCaseNumber", s);
                    System.out.println("Data : " + s);
                    break;

                case "Lender Case Number ":
                    s = searchInfile(sPath, "Lender Case Number", 1);
                    System.out.println("Data : " + s);
                    PDFData.put("LenderCaseNumber", s);
                    break;
                case "Amount ":
                    s = searchInfile(sPath, "Amount", 2);
                    PDFData.put("Amount", s);
                    System.out.println("Data : " + s);
                    break;
                case "Interest Rate ":
                    s = searchInfile(sPath, "Interest Rate", 2);
                    PDFData.put("InterestRate", s);
                    System.out.println("Data : " + s);
                    break;
                case "No. of Months ":
                    String sLoanTerm = searchInfile(sPath, "No. of Months ", 1);
                    sLoanTerm = sLoanTerm.trim();
                    sLoanTerm = sLoanTerm.substring(0, 3);
                    System.out.println("Data : " + sLoanTerm);
                    PDFData.put("LoanTerm", sLoanTerm);
                    String sAmortTerm = searchInfile(sPath, "No. of Months ", 2);
                    sAmortTerm = sAmortTerm.trim();
                    sAmortTerm = sAmortTerm.substring(0, 3);
                    PDFData.put("AmortTerm", sAmortTerm);
                    System.out.println("Data : " + sAmortTerm);
                    break;

                case "Amortization Type ":
                    // CheckBox
                    break;
                case "Subject Property Address (street, city, state & ZIP) ":
                    s = searchInfile(sPath, "Subject Property Address (street, city, state & ZIP)", 1);
                    iFoundAtLine++;
                    s = s.trim();
                    PDFData.put("PropertyAddress", s);
                    System.out.println("Data : " + s);
                    break;
                case "No. of Units ":
                    s = searchInfile(sPath, "No. of Units", 1);
                    s = s.trim();
                    PDFData.put("NoOfUnits", s);
                    System.out.println("Data : " + s);
                    break;
            }

        }

        System.out.println("String found at : " + iFoundAtLine);
        return PDFData;
    }

    /***************************************************************
     * @author Akshay
     * @Description Read PDF file and convert it into Text file
     **************************************************************/
    public static void readPDF(String sPDFFilePath)
    {

        try
        {
            PDDocument document = null;
            document = PDDocument.load(new File(sPDFFilePath));
            document.getClass();
            if (!document.isEncrypted())
            {
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);
                PDFTextStripper Tstripper = new PDFTextStripper();
                String pdfString = Tstripper.getText(document);

                File fOut = new File(System.getProperty("user.dir")+"\\outpdf.txt");
                FileWriter fooStream = new FileWriter(fOut, false);
                fooStream.write(pdfString);
                fooStream.close();

                // System.out.println("Text:" + st);
                /*
                 * PDAcroForm form =
                 * document.getDocumentCatalog().getAcroForm(); PDField field =
                 * form.getField("FHA"); System.out.println(((PDCheckBox)
                 * field).isChecked());
                 */

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    /***************************************************************
     * @author Akshay
     * @Description Search String in text file generated from PDF file
     * @param sPath
     *            of text file
     * @param sText
     *            to search
     * @param skip
     *            lines to get data below Header
     * @return Data corresponding to the search text(Header)
     **************************************************************/
    public static String searchInfile(String sPath, String sText, int iToIncrement) throws IOException
    {
        int i = 0, iLineNumber = 0;
        String sdata = "";
        FileReader fileReader = null;
        String line = null;
        int counter = 0;
        Scanner scan = new Scanner(new File(sPath));
        while (scan.hasNext())
        {
            line = scan.nextLine().toString();
            if (line.contains(sText))
            {
                System.out.println("Header: " + line);
                iLineNumber = i;
                break;

            }
            i++;
        }
        iLineNumber = iLineNumber + iToIncrement;
        // FileReader reads text files in the default encoding.
        fileReader = new FileReader(sPath);
        // Always wrap FileReader in BufferedReader.

        BufferedReader bufferedReader = new BufferedReader(fileReader);

        while ((line = bufferedReader.readLine()) != null)
        {
            counter++;

            if (counter == iLineNumber)
            {
                sdata = bufferedReader.readLine();
                break;
            }

        }
        sdata = sdata.trim();
        return sdata;

    }
}
