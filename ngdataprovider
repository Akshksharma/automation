package base;

import java.lang.reflect.Method;
import org.testng.annotations.DataProvider;
import utility.ConfigProperties;
import utility.ExcelUtils;
import utility.Log;

public class TestNGDataProvider {
	static ConfigProperties configReader = new ConfigProperties();

	/**
	 * Imports data from Excel File and populates an array
	 * @param method Reflection Method
	 * @return Excel data in form Array
	 * @throws Exception
	 */
	@DataProvider(name = "globalDataExcel", parallel = true)
	public static Object[][] getDataProvider(Method method) throws Exception {

		try {
			Object[][] retDataArr = ExcelUtils.getXLArray(configReader.getTestDataFile(), method.getName());
			return (retDataArr);
		} catch (NullPointerException ne) {
			System.out.println("File doesn't exists. Please check the File name matches to the method name.");
			Log.error(ne.getMessage());
			throw (ne);
		} catch (Exception e) {
			Log.error(e.getMessage());
			throw (e);
		}
	}

	/**
	 * Imports data from CSV File and populates an array
	 * @param method Reflection Method
	 * @return CSV data in form Array
	 * @throws Exception
	 */
	@DataProvider(name = "globalData")
	public static Object[][] getDataProviderFromCSV(Method method) throws Exception {

		String className = method.getDeclaringClass().getName();
		String cname = className.replace(".", " ");
		String[] actClassname = cname.split(" ");

		try {
			Object[][] retDataArr = CSVReadr.getCSVData(configReader.getTestDataPath(), actClassname[1],
					method.getName());
			return (retDataArr);
		} catch (NullPointerException ne) {
			System.out.println("File doesn't exists. Please check the File name matches to the method name.");
			Log.error(ne.getMessage());
			throw (ne);
		} catch (Exception e) {
			Log.error(e.getMessage());
			throw (e);
		}
	}
}
