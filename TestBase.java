package base;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.xalan.templates.ElemTemplate;
import org.json.simple.JSONObject;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.converters.ExtentHtmlLogConverter;
import com.csvreader.CsvWriter;
import com.google.common.base.Function;
import utility.ConfigProperties;
import utility.Log;
import utility.Utils;

/*
 * @author=sameer
 */
public class TestBase {
	private static HashMap<Long, WebDriver> driverMap;
	private WebDriver webDriver;
	public static long id = Thread.currentThread().getId();
	static String browser;
	DateFormat dateFormat;
	public Date date;
	public static GlobalData data = new GlobalData();
	public static LoanData loanData;
	public static BorrowerData borrowerData;
	private static String sFinalResult = "PASSED!";
	private static boolean bResult;
	private static String sResult = "FAILED!";
	public static JSONObject sysConfig;
	public static JSONObject elementConfig;
	public static String sConfigName = null;
	private static String sTestSuiteName = null;
	private static String sTestCaseName = null;
	private static String sTestCaseDesc = null;
	private static String sElementMapping = null;
	private static boolean bDebug = false;
	public static ConfigProperties configProperties;
	private String resourcePath = "src\\test\\resources\\";
	public static String[][] performance = new String[200][4];

	public String getResourcePath() {
		return resourcePath;
	}

	public static void setTestSuiteName(String sTSName) {
		sTestSuiteName = sTSName;
	}

	public static String getTestSuiteName() {
		return sTestSuiteName;
	}

	public static void setTestcaseName(String sTCName) {
		sTestCaseName = sTCName;
	}

	public static String getTestcaseName() {
		return sTestCaseName;
	}

	public static void setTestcaseDesc(String sTCDesc) {
		sTestCaseDesc = sTCDesc;
	}

	public static String getTestcaseDesc() {
		return sTestCaseDesc;
	}

	public static void setResult(String sRes) {
		sResult = sRes;
	}

	public static String getResult() {
		return sResult;
	}

	public static void setResultFlag(boolean bRes) {
		bResult = bRes;
	}

	public static void setElementMapping(String sElementMap) {
		sElementMapping = sElementMap;
	}

	/**
	 * @return the sFinalResult
	 */
	public static String getsFinalResult() {
		return sFinalResult;
	}

	/**
	 * @param sFinalResult
	 *            the sFinalResult to set
	 */
	public static void setsFinalResult(String sFinalResult) {
		TestBase.sFinalResult = sFinalResult;
	}

	public static String getElementMapping() {
		return sElementMapping;
	}

	public static void setFinalResult(String sElementMap) {
		sFinalResult = sElementMap;
	}

	public static String getFinalResult() {
		return sFinalResult;
	}

	public static boolean getResultFlag() {
		return bResult;
	}

	public static String ConfigPath = "foo";

	public static String getSuiteResult() {
		String sRes = null;
		if (getResultFlag() == true) {
			sRes = "PASSED!";
		} else {
			sRes = "FAILED!";
		}
		return sRes;

	}

	public static void setDebugMessages(boolean bDebugMsg) {
		bDebug = bDebugMsg;
	}

	public static boolean getDebugMessages() {
		return bDebug;
	}

	/**
	 * @param select
	 *            Web Element for Select
	 * @param option
	 *            Option to be selected
	 */
	public static void selectElementUsingJavaScript(WebElement select, String option) {
		JavascriptExecutor executor = (JavascriptExecutor) getDriverInstance();
		executor.executeScript(
				"var select = arguments[0]; for(var i = 0; i < select.options.length; i++){ if(select.options[i].text == arguments[1]){ select.options[i].selected = true; } }",
				select, option);
	}

	/**
	 * Entering Value in Text box
	 * 
	 * @param element
	 *            WebElement defined in respective page
	 * @param sValue
	 *            Value to be entered
	 */
	public void enterValueInTextBox(WebElement element, String sValue) {

		element.click();
		element.clear();
		element.sendKeys(sValue);
	}

	/**
	 * Selecting index
	 * 
	 * @param element
	 *            WebElement of Select
	 * @param value
	 *            Index value to be selected
	 */
	public void selectInt(WebElement element, int value) {
		Select selchannel2 = new Select(element);
		selchannel2.selectByIndex(value);
	}

	/**
	 * To read and initialize the configuration files.
	 * 
	 * @throws FileNotFoundException
	 */
	@BeforeSuite
	public void testConfigReader() throws FileNotFoundException {
		configProperties = new ConfigProperties();
		ConfigPath = resourcePath + configProperties.configPath;
		DOMConfigurator.configure(".\\SetupFiles\\log4j.xml");
		dateFormat = new SimpleDateFormat("M/d/yyyy");
		try {
			Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		date = new Date();
		System.out.println(dateFormat.format(date));

		if (configProperties.getApplication().equalsIgnoreCase(Application.Forward.toString())) {
			setElementMapping(resourcePath + configProperties.elementMappingForward);
		}

	}

	/**
	 * Navigate to Link
	 * 
	 * @param sURLLink
	 *            Link to be navigated
	 */
	public void NavigatetoLink(String sURLLink) {
		try {
			long start = System.currentTimeMillis();
			waitFor(2);
			getDriverInstance().get(sURLLink);
			long finish = System.currentTimeMillis();
			long totalTime = (finish - start);
			String sTotalTime = Long.toString(totalTime);
			System.out.println("Total Time for page load - " + sTotalTime + " ms");
			Log.info("Total Time for page load - " + sURLLink + " " + sTotalTime + " ms");
			try {
				Alert alert = getDriverInstance().switchTo().alert();
				Log.warn("Modal dialog " + alert.getText() + " appeared while navigating to " + sURLLink);
				alert.accept();
			} catch (NoAlertPresentException e) {
				// no action needed
			}
		} catch (AssertionError e) {
			Log.warn("ERROR: Cannot navigate to link");
		}
	}

	/**
	 * To return the URL as per environment.
	 * 
	 * @return
	 */
	public String getUrl() {
		String url = null;
		switch (Application.valueOf(configProperties.getApplication())) {
		case Forward:
			switch (Environment.valueOf(configProperties.getEnvironment())) {
			case QA01:
				url = configProperties.getQa01MAXUrl();
				break;
			case QA02:
				url = configProperties.getQa02MAXUrl();
				break;
			case UAT01:
				url = configProperties.getUat01MAXUrl();
				break;
			case PreProd:
				url = configProperties.getPreProdMAXUrl();
				break;
			case UAT02:
				url = configProperties.getUat02MAXUrl();
				break;
			case Prod:
				url = configProperties.getProdMAXUrl();
				break;
			case DEV:
				url = configProperties.getDevMAXUrl();
				break;
			default:
				break;
			}
			break;

		case LendingPortal:
			switch (Environment.valueOf(configProperties.getEnvironment())) {
			case QA01:
				url = configProperties.getQa01MAXPortalUrl();
				break;
			case QA02:
				url = configProperties.getQa02MAXPortalUrl();
				break;
			case UAT01:
				url = configProperties.getUat01MAXPortalUrl();
				break;
			case UAT02:
				url = configProperties.getUat02MAXPortalUrl();
				break;
			case PreProd:
				url = configProperties.getPreProdMAXPortalUrl();
				break;
			case Prod:
				url = configProperties.getProdMAXPortalUrl();
				break;
			case DEV:
				url = configProperties.getDevMAXPortalUrl();
				break;

			default:
				break;

			}
			break;

		case AdminPortal:
			switch (Environment.valueOf(configProperties.getEnvironment())) {
			case QA01:
				url = configProperties.getQa01MAXAdminUrl();
				break;
			case QA02:
				url = configProperties.getQa02MAXAdminUrl();
				break;
			case UAT01:
				url = configProperties.getUat01MAXAdminUrl();
				break;
			case PreProd:
				url = configProperties.getPreProdMAXAdminUrl();
				break;
			case Prod:
				url = configProperties.getProdAdminUrl();
				break;

			case UAT02:
				url = configProperties.getUat02MAXAdminUrl();
				break;

			default:
				break;

			}
			break;

		case OrgAdminURL:
			switch (Environment.valueOf(configProperties.getEnvironment())) {
			case QA01:
				url = configProperties.getQa01OrgAdminUrl();
				break;
			case QA02:
				url = configProperties.getQa02OrgAdminUrl();
				break;
			case UAT01:
				url = configProperties.getUat01OrgAdminUrl();
				break;
			case UAT02:
				url = configProperties.getUat02OrgAdminUrl();
				break;
			case PreProd:
				url = configProperties.getPreProdOrgAdminUrl();
				break;
			case Prod:
				url = configProperties.getProdOrgAdminUrl();
				break;

			default:
				break;
			}
			break;
		default:
			throw new RuntimeException("Un supported application");
		}
		System.out.println("Launching the url  :" + url);
		return url;
	}

	/**
	 * To return the database server name as per the environment.
	 */
	public static String getDbServer() {

		String dbServer = null;
		switch (Environment.valueOf(configProperties.getEnvironment())) {
		case QA01:
			dbServer = configProperties.getDatabaseServerNameQA01();
			break;
		case QA02:
			dbServer = configProperties.getDatabaseServerNameQA02();
			break;
		case UAT01:
			dbServer = configProperties.getDatabaseServerNameUAT01();
			break;
		case UAT02:
			dbServer = configProperties.getDatabaseServerNameUAT02();
			break;
		case PreProd:
			dbServer = configProperties.getDatabaseServerNamePreProd();
			break;
		case Prod:
			dbServer = configProperties.getDatabaseServerNamePreProd();
			break;
		default:
			throw new RuntimeException("Un supported Environment.");

		}
		Assert.assertNotNull(dbServer, "database server name cannot be null.");
		return dbServer;
	}

	/**
	 * Return the driver instance.
	 * 
	 * @return
	 */
	public static WebDriver getDriverInstance() {
		return driverMap.get(id);
	}

	// **********************************************************************
	/**
	 * @author bhargavs To launch the browser.
	 * @param browser
	 * @return
	 * @throws Exception
	 */
	@BeforeClass
	// @Parameters("browser")
	
	public void createWebDriver() throws Exception {
	    ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("E://extent.html");	 
	    

	    // initialize ExtentReports and attach the HtmlReporter
	    ExtentReports extent = new ExtentReports();

	    // attach only HtmlReporter
	    extent.attachReporter(htmlReporter);	        
	                
		browser = System.getProperty("browser");
		configProperties.setMortgageTypeOrLoaNnumberType("Conventional");
		if (webDriver == null) {
			try {
				switch (BrowserTypes.valueOf(browser)) {
				case CHROME:
					System.setProperty("webdriver.chrome.driver", ".\\SetupFiles\\chromedriver.exe");
					ChromeOptions options = new ChromeOptions();
					options.addArguments("chrome.switches", "--disable-extensions");
					DesiredCapabilities capabilitiesChrome = DesiredCapabilities.chrome();
					String downloadFilepath = System.getProperty("user.dir") + "\\src\\test\\resources\\Download";
					HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
					capabilitiesChrome.setPlatform(Platform.WINDOWS);
					if (configProperties.isRemote()) {
						capabilitiesChrome.setBrowserName("chrome");
						webDriver = new RemoteWebDriver(new URL(configProperties.getNodeUrl()), capabilitiesChrome);
						System.out.println("Launching grid for Chrome browser.");
					} else {
						chromePrefs.put("download.default_directory", downloadFilepath);
						chromePrefs.put("credentials_enable_service", false);
						chromePrefs.put("profile.password_manager_enabled", false);
						options.setExperimentalOption("prefs", chromePrefs);
						capabilitiesChrome.setCapability(ChromeOptions.CAPABILITY, options);
						webDriver = new ChromeDriver(capabilitiesChrome);
						System.out.println("Going to launch Chrome driver!");
					}

					Reporter.log("Going to launch Chrome driver!");
					break;

				case FIREFOX:
					System.setProperty("webdriver.gecko.driver", ".\\SetupFiles\\geckodriver.exe");
					DesiredCapabilities capabilities = DesiredCapabilities.firefox();
					capabilities.setCapability("marionette", true);
					webDriver = new FirefoxDriver(capabilities);
					System.out.println("Going to launch Firefox driver!");
					break;

				case IE:
					System.setProperty("webdriver.ie.driver", ".\\SetupFiles\\IEDriverServer.exe");
					DesiredCapabilities capabilitiesIE = DesiredCapabilities.internetExplorer();
					capabilitiesIE.setPlatform(org.openqa.selenium.Platform.WINDOWS);
					if (configProperties.isRemote()) {
						System.out.println("Inside remote::" + configProperties.getNodeUrl());
						capabilitiesIE.setPlatform(Platform.WINDOWS);
						capabilitiesIE.setCapability(CapabilityType.BROWSER_NAME, "internet explorer");
						capabilitiesIE.setCapability(CapabilityType.VERSION, "11");
						capabilitiesIE.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
						capabilitiesIE.setJavascriptEnabled(true);
						capabilitiesIE.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
						webDriver = new RemoteWebDriver(new URL(configProperties.getNodeUrl()), capabilitiesIE);
						System.out.println("Launching grid for IE browser.");
					} else {
						capabilitiesIE.setCapability(
								InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
						webDriver = new InternetExplorerDriver(capabilitiesIE);
						System.out.println("Going to launch IE driver!");
					}
					Reporter.log("Going to launch IE driver!");
					break;

				default:
					new RuntimeException("Unsupported browser type");
				}
				webDriver.manage().window().maximize();
				webDriver.manage().timeouts().implicitlyWait(TimeConstants.implicitWaitTime, TimeUnit.SECONDS);
				driverMap = new HashMap<Long, WebDriver>();
				driverMap.put(id, webDriver);
			} catch (Exception e) {
				System.out.println("Unable to acquire the webdriver ." + e);
				throw e;
			}
		}

	}

	/**
	 * Explicit wait.
	 */
	public static void waitForPageLoad() {
		WebDriverWait wait = new WebDriverWait(getDriverInstance(), TimeConstants.pageLoadTime);
		wait.until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver) {
				return String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
						.equals("complete");
			}
		});
	}

	/**
	 * Static wait.
	 * 
	 * @param iSeconds
	 * @return
	 */
	public static boolean waitFor(int iSeconds) {
		try {
			Thread.sleep(iSeconds * 1000);
		} catch (Exception e) {
			Log.error("Not able to Wait --- " + e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * 
	 * @param element
	 */
	public static void waitForElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(getDriverInstance(), 100);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public static void waitForElementClickable(WebElement element) {
		WebDriverWait wait = new WebDriverWait(getDriverInstance(), 100);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	/**
	 * @author bhargavs
	 * @param element
	 * 
	 */

	public static String randomNameSuffix() {
		String sName = null;
		int r1 = (int) (Math.random() * 1000);
		sName = Integer.toString(r1);
		return sName;
	}

	public static String getDataUsingJExecutor(String element, String selector) {

		String sValue = null;
		WebDriver driver = TestBase.getDriverInstance();
		try {
			if (selector.equalsIgnoreCase("Id")) {
				sValue = (String) ((JavascriptExecutor) driver)
						.executeScript("return document.getElementById('" + element + "').value");
			} else if (selector.equalsIgnoreCase("xpath")) {
				sValue = (String) ((JavascriptExecutor) driver)
						.executeScript("return document.getElementByXPath('" + element + "').value");
			}

		} catch (WebDriverException driverException) {

		}
		return sValue;
	}

	/**
	 * Wait for Element to be clickable
	 * 
	 * @param element
	 *            WebElement
	 */
	public static void clickOnEnable(WebElement element) {
		System.out.println("Clicking on element once enable" + element);
		try {
			waitForElementClickable(element);
			element.click();
		} catch (WebDriverException e) {
			Assert.assertTrue(false, "Unable to click the element.");
		}
	}

	/**
	 * Enter value after visible
	 * 
	 * @param element
	 *            Web Element
	 * @param keys
	 *            Text to be entered
	 */
	public static void sendKeyOnEnable(WebElement element, String keys) {
		System.out.println("Entering value as" + keys);
		waitForElementVisible(element);
		element.sendKeys(keys);
	}

	/**
	 * Entering value in text box once enable
	 * 
	 * @param element
	 *            Web Element
	 * @param keys
	 *            Text to be send
	 */
	public static void sendKeyOnEnable(WebElement element, Keys keys) {
		waitForElementVisible(element);
		System.out.println("Entering values in element " + element);
		element.sendKeys(keys);
	}

	/**
	 * SendKey On Enableand Clear
	 * 
	 * @param element
	 *            Web Element
	 * @param keys
	 *            Keys
	 */
	public static void sendKeyOnEnableandClear(WebElement element, String keys) {
		waitForElementVisible(element);
		System.out.println("Entering value in element " + element + " as " + keys);
		element.clear();
		element.sendKeys(keys);
	}

	/**
	 * @author bhargavs
	 * @param element
	 * 
	 */
	public static void clickOnVisible(WebElement element) {
		try {
			waitForElementVisible(element);
			element.click();
		} catch (WebDriverException e) {
			Assert.assertTrue(false, "Unable to click the element.");
		}
	}

	/**
	 * Wait for Element to be visible on page
	 * 
	 * @param element
	 *            Web Element
	 */
	public static void waitForElementVisible(WebElement element) {
		System.out.println("Wait for Element Visible" + element);
		WebDriverWait wait = new WebDriverWait(getDriverInstance(), 60);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	/**
	 * @author bhargavs To validate if element is present.
	 * @param element
	 * @param selector
	 * @return
	 */
	public boolean isElementPresent(String element, String selector) {
		boolean isPresent = false;
		try {
			if (selector.equalsIgnoreCase("css")) {
				getDriverInstance().findElement(By.cssSelector(element));
			} else if (selector.equalsIgnoreCase("xpath")) {
				getDriverInstance().findElement(By.xpath(element));
			} else if (selector.equalsIgnoreCase("id")) {
				getDriverInstance().findElement(By.xpath(element));
			}
			isPresent = true;
		} catch (WebDriverException driverException) {
			isPresent = false;
		}
		return isPresent;
	}

	/**
	 * @author bhargavs To validate if element is present.
	 * @param webElement
	 * @return
	 */
	public boolean isElementPresent(WebElement webElement) {
		boolean isPresent = false;
		try {
			waitForElementVisible(webElement);
			isPresent = true;
		} catch (WebDriverException driverException) {
			isPresent = false;
		}
		return isPresent;
	}

	public void selectByValue(WebElement element, String value) {
		Select selValue = new Select(element);
		selValue.selectByValue(value);
	}

	public void selectLendingDropDown(WebElement element, String sElementText) {
		waitForElementVisible(element);
		System.out.println("Selecting text " + sElementText + " from elment " + element);
		Select dropElement = new Select(element);
		dropElement.selectByVisibleText(sElementText);
	}

	public void selectByIndex(WebElement element, int iIndex) {
		waitForElementVisible(element);
		Select dropElement = new Select(element);
		dropElement.selectByIndex(iIndex);

	}

	public static boolean verifyOnPage(String sPageName) {
		boolean bFound = false;
		if (getDriverInstance().getTitle().equals(sPageName)) {
			bFound = true;
		}
		return bFound;
	}

	/**
	 * Wait for invisibility of Element
	 * 
	 * @param locatorname
	 *            Locator Name
	 * @param timeout
	 *            Timeout
	 * @return boolean value based on visibility
	 */
	public static boolean waitForInvisibilityOfElement(String locatorname, int timeout) {
		System.out.println("Wait for Invisibility of Element " + locatorname);
		WebDriverWait wait = new WebDriverWait(TestBase.getDriverInstance(), timeout);
		return wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(locatorname)));
	}

	/**
	 * Returns the property value from JSON file.
	 * 
	 * @param key
	 * @return
	 */
	public static String getJSONProperty(String key) {
		String propVlaue = null;
		try {
			propVlaue = sysConfig.get(key).toString();
		} catch (Exception e) {
			System.out.println("Entered key value not found in the input file.");
			throw e;
		}
		return propVlaue;
	}

	public static String getCurrDate() {
		String CurrDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/YYYY");
		Date date = new Date();
		CurrDate = sdf.format(date);

		return CurrDate;
	}

	/**
	 * Scroll down to WebElement
	 * 
	 * @param element
	 *            Web Element
	 */
	public static void scroll_element_into_view(WebElement element) {
		waitFor(4);
		System.out.println("Scrolling into element" + element);
		int Axis = (element.getLocation().getY() - 100);
		JavascriptExecutor js = (JavascriptExecutor) getDriverInstance();
		js.executeScript("javascript:window.scrollTo(0," + Axis + ");");
	}

	@AfterClass
	public void destroy() {
		getDriverInstance().quit();
		driverMap = null;
	}

	/**
	 * Upload Pricing File
	 * 
	 * @param sFilePath
	 *            Path of File
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public static void uploadPricingFile(String sFilePath) throws InterruptedException, AWTException {
		StringSelection path = new StringSelection(sFilePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(path, null);

		waitFor(2);

		System.out.println("Pasting the path in the File name field");
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		System.out.println("Click the Open button so as to upload file");
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		try {
			waitFor(3);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	/*
	 * @author : Akshay Sharma
	 * 
	 * @description Validates all the links present on a webpage
	 */
	public static void validateLinks() {
		WebDriver driver = getDriverInstance();
		String sCurrURL = driver.getCurrentUrl();
		try {
			LinkValidator.verifyLinks(driver, sCurrURL);
		} catch (IOException e) {

			e.printStackTrace();
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
	}

	public String getBank() {

		String envBank = null;
		String exeEnv = System.getProperty("environment");
		if (exeEnv.equalsIgnoreCase("qa01"))
			envBank = ApplicationConstants.BANK_QA01;
		if (exeEnv.equalsIgnoreCase("qa02"))
			envBank = ApplicationConstants.BANK_QA02;
		if (exeEnv.equalsIgnoreCase("uat01"))
			envBank = ApplicationConstants.BANK_UAT01;
		if (exeEnv.equalsIgnoreCase("preprod01"))
			envBank = ApplicationConstants.BANK_PREPROD01;

		return envBank;
	}

	/**
	 * Selecting value from drop down based on Text
	 * 
	 * @param sItemName
	 *            Name of Text to be selected from drop down
	 */
	public static void selectDropdownValue(String sItemName) {
		String sXpath = "";
		sXpath = "//div[@class='dropdown-menu open']//*[text()='" + sItemName + "']";
		TestBase.getDriverInstance().findElement(By.xpath(sXpath)).click();
		waitFor(2);
	}

	public static void selectItemdropdownlist(WebElement weDD, String sListItem) {
		// ***** FORWARD DROP DOWN *****
		System.out.println("Inside selectItemdropdownlist method on TestBase");
		List<WebElement> allOptions = Utils.getDropdownList(weDD);
		java.util.Iterator<WebElement> i = allOptions.iterator();
		while (i.hasNext()) {
			WebElement row = i.next();
			System.out.println("Found: " + row.getAttribute("label"));
			if (row.getAttribute("label").equals(sListItem)) {
				row.click();
				waitFor(3);
				// row.sendKeys(Keys.ENTER);
				break;
			}
		}
		System.out.println("  selectItemdropdownlist method  execution completed");
	}

	public void SelectDropdownValues(String elementId, String sItemName) {

		WebDriver driver = TestBase.getDriverInstance();
		String sXpath = "";
		sXpath = ".//*[@id='" + elementId + "']/option[@label='" + sItemName + "']";
		driver.findElement(By.xpath(sXpath)).click();
		Utils.waitFor(2);
	}

	/**
	 * Selecting value from drop down based on value
	 * 
	 * @param elementId
	 *            Element ID
	 * @param sItemName
	 *            Value to be selected from drop down
	 */
	public void SelectDropdownValuesbyvalue(String elementId, String sItemName) {
		System.out.println("Selecting Value " + sItemName + " from Select Element " + elementId);
		WebDriver driver = TestBase.getDriverInstance();
		String sXpath = "";
		sXpath = ".// *[@id='" + elementId + "']/option[@value='string:" + sItemName + "']";
		driver.findElement(By.xpath(sXpath)).click();
		Utils.waitFor(2);
	}

	public void enterAmountinTextBox(WebElement element1, WebElement element, String sValue) {
		clickOnEnable(element1);
		element.clear();
		sendKeyOnEnable(element, sValue);
	}

	/**
	 * Update Loan no in CSV
	 * 
	 * @param sLoanNumber
	 */
	public static void UpdateLoanNumberInCSV(String sLoanNumber) {
		System.out.println("Reading NewLoanNumbersCreated CSV file");
		String filepath = System.getProperty("user.dir") + "\\NewLoanNumbersCreated.csv";
		boolean alreadyExists = new File(filepath).exists();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/YYYY");
		String CurrDate = sdf.format(date);
		String environment = System.getProperty("environment");
		String application = System.getProperty("application");
		System.out.println("Writing Loan no in NewLoanNumbersCreated CSV");
		try {
			CsvWriter csvOutput = new CsvWriter(new FileWriter(filepath, true), ',');
			if (!alreadyExists) {
				csvOutput.write("DATE");
				csvOutput.write("ENV");
				csvOutput.write("Application");
				csvOutput.write("LoanNumber");
				csvOutput.endRecord();
			}
			csvOutput.write(CurrDate);
			csvOutput.write(environment);
			csvOutput.write(application);
			csvOutput.write(sLoanNumber);
			csvOutput.endRecord();
			csvOutput.close();
			System.out.println("Closing CSV file");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save Page
	 */
	public static void savePage() {
		if (isAlertPresent()) {
			getDriverInstance().switchTo().alert().dismiss();
		} else {
			clickElementUsingJavaScript(
					getDriverInstance().findElement(By.xpath("(//button[contains(text(),'Save')])[1]")));
			waitForPageLoad();
			System.out.println(getDriverInstance().getTitle() + ": Page Saved Sucessfully");
			Reporter.log(getDriverInstance().getTitle() + " : Page Saved Sucessfully");
			Reporter.log("No Alert Present!Page Saved Sucessfully");
			System.out.println("No Alert Present!Page Saved Sucessfully");
		}
	}

	public static boolean isAlertPresent() {
		boolean bAlert = false;

		try {
			Alert windowAlert = new WebDriverWait(getDriverInstance(), 2).until(ExpectedConditions.alertIsPresent());
			if (windowAlert != null) {
				System.out.println("alert was present");
				bAlert = true;

			} else {
				throw new Throwable();
			}
		} catch (Throwable e) {
			System.out.println("alert was not present...Proceed with save");

		}

		return bAlert;
	}

	/**
	 * Clicking Element using Java Script
	 * 
	 * @param element
	 *            Web Element to be clicked
	 */
	public static void clickElementUsingJavaScript(WebElement element) {
		JavascriptExecutor executor = (JavascriptExecutor) getDriverInstance();
		executor.executeScript("arguments[0].click();", element);
	}

	public static void checkFailSavePopup() {
		if (isAlertPresent()) {

			getDriverInstance().switchTo().alert().dismiss();
			savePage();
		}

	}
}
