package utility;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import com.google.common.base.Function;

import base.ApplicationConstants;
import base.Environment;
import base.TestBase;
import base.TimeConstants;

public class Utils {
	public static WebDriver driver;
	public static WebElement element = null;
	public static Hashtable<String, String> loanregistration = new Hashtable<String, String>();
	public static String globalFileName = "/TestData/global.txt";

	public static WebDriver InitBrowser(String sBrowser) throws Exception {
		try {
			if (sBrowser.equals("IE")) {
				System.setProperty("webTestBase.getDriverInstance().ie.TestBase.getDriverInstance()",
						"TestBase.getDriverInstance()s\\IETestBase.getDriverInstance()Server.exe");
				System.out.println("Going to launch IE");
				Reporter.log("Going to launch IE");
				WebDriver driver = new InternetExplorerDriver();
				driver = new InternetExplorerDriver();
				driver = new InternetExplorerDriver();
			} else if (sBrowser.equals("Chrome")) {
				System.setProperty("webTestBase.getDriverInstance().chrome.TestBase.getDriverInstance()",
						"TestBase.getDriverInstance()s\\chromeTestBase.getDriverInstance().exe");
				System.out.println("Going to launch CHROME");
				Reporter.log("Going to launch CHROME");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("chrome.switches", "--disable-extensions");

				WebDriver driver = new ChromeDriver(options);
			} else {
				WebDriver driver = new InternetExplorerDriver();

			}

			Log.info(sBrowser + " web TestBase.getDriverInstance() instantiated");
			TestBase.getDriverInstance().manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);

		} catch (Exception e) {
			Log.error("Class Utils | Method InaitBrowser | Exception desc : " + e.getMessage());
			throw e;
		}
		return TestBase.getDriverInstance();
	}

	public static void waitForElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(TestBase.getDriverInstance(), 100);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public static void waitForElementClickable(WebElement element) {
		WebDriverWait wait = new WebDriverWait(TestBase.getDriverInstance(), 100);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public static void waitForElementVisable(WebElement element) {
		WebDriverWait wait = new WebDriverWait(TestBase.getDriverInstance(), 60);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public static void gotoMAXMenuItem(String sMenuTab, String sMenuItem, String sTabName) throws Exception {

		try {

			if (TestBase.getDriverInstance().getTitle().contains(sMenuItem)) {
				if (sTabName != "") {
					TestBase.getDriverInstance().findElement(By.linkText(sTabName)).click();
				}
			} else {
				waitFor(2);
				waitForElementClickable(TestBase.getDriverInstance()
						.findElement(By.xpath("//*[contains(text(), '" + sMenuTab + "')]")));
				element = TestBase.getDriverInstance()
						.findElement(By.xpath("//*[contains(text(), '" + sMenuTab + "')]"));
				element.click();
				waitForPageLoad();
				waitForElementClickable(TestBase.getDriverInstance()
						.findElement(By.xpath("//*[contains(text(), '" + sMenuItem + "')]")));
				element = TestBase.getDriverInstance()
						.findElement(By.xpath("//*[contains(text(), '" + sMenuItem + "')]"));
				element.click();
				waitFor(5);
				if (sTabName != "") {
					TestBase.getDriverInstance().findElement(By.linkText(sTabName)).click();
				}
				waitForPageLoad();
			}

		} catch (Exception e) {
			element = TestBase.getDriverInstance().findElement(By.xpath("//*[contains(text(), '" + sMenuTab + "')]"));
			element.click();
			throw (e);
		}
		// return sTabName;
	}

	public static void waitForPageLoad() {

		WebDriverWait wait = new WebDriverWait(TestBase.getDriverInstance(), 70);
		wait.until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver) {
				return String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
						.equals("complete");
			}
		});
	}

	public static void takeScreenshot(WebDriver driver, String sTestCaseName) throws Exception {
		try {
			if (sTestCaseName.equals("") || sTestCaseName.equals(null)) {
				sTestCaseName = "UNKNOWN_TESTCASE_";
			}
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile,
					new File(".\\Screenshots\\" + sTestCaseName + System.currentTimeMillis() + ".jpg"));
			System.out.println("Screen shot " + sTestCaseName + " taken.");
			Reporter.log("Screen shot " + sTestCaseName + " taken.");
		} catch (Exception e) {
			Log.error("Exception occured while capturing ScreenShot : " + e.getMessage());
			throw e;
		}
	}

	public static boolean waitFor(int iSeconds) {
		try {
			Thread.sleep(iSeconds * 1000);
		} catch (Exception e) {
			Log.error("Not able to Wait --- " + e.getMessage());
			return false;
		}
		return true;
	}

	public static void setDropdownbyIndex(WebElement ElementID, int iIndex) throws Exception {
		Actions actions = new Actions(TestBase.getDriverInstance());
		try {
			actions.moveToElement(ElementID);
			waitFor(1);
			for (int i = 0; i < iIndex; i++) {
				actions.sendKeys(Keys.ARROW_DOWN);
			}
			actions.sendKeys(Keys.ENTER);
			actions.build().perform();
		} catch (Exception e) {
			Log.error(e.getMessage());
			throw (e);
		}
	}

	static public boolean verifyEquals(String found, String expected, String message) {
		boolean result = true;
		try {
			Assert.assertEquals(found, expected, message);
			return result;

		} catch (AssertionError e) {
			System.out.println("Error: " + e.getMessage());
			result = false;
			Log.error(e.getMessage());
			throw e;
		}
	}

	static public void arraytoCSVfile(String[][] saArr, String sFilename) throws IOException {
		BufferedWriter br = new BufferedWriter(new FileWriter(sFilename));
		StringBuilder sb = new StringBuilder();
		int iLen = saArr.length;
		int jLen = saArr[0].length;
		for (int i = 0; i < iLen; i++) {
			for (int j = 0; j < jLen; j++) {
				// sLineout = sLineout + saArr[i][j]+ ",";
				sb.append(saArr[i][j]);
				sb.append(",");
			}
			sb.append("\n");
		}
		// send line to textfile
		System.out.println("ARRAY: " + sb);
		br.write(sb.toString());
		br.close();
	}

	static public void NavigatetoLink(String sURLLink) {
		try {
			long start = System.currentTimeMillis();
			waitFor(2);
			TestBase.getDriverInstance().get(sURLLink);
			long finish = System.currentTimeMillis();
			long totalTime = (finish - start);
			String sTotalTime = Long.toString(totalTime);
			System.out.println("Total Time for page load - " + sTotalTime + " ms");
			Log.info("Total Time for page load - " + sURLLink + " " + sTotalTime + " ms");
			Reporter.log("Total Time for page load - " + sURLLink + " " + sTotalTime + " ms");
			try {
				Alert alert = TestBase.getDriverInstance().switchTo().alert();
				Log.warn("Modal dialog " + alert.getText() + " appeared while navigating to " + sURLLink);
				alert.accept();
			} catch (NoAlertPresentException e) {
				// no action needed
			}
		} catch (AssertionError e) {
			Log.warn("ERROR: Cannot navigate to link");
		}
	}

	public static String randomSSN() {
		String sSSN = null;
		int r1 = (int) (Math.random() * (999 - 1)) + 100;
		int r2 = (int) (Math.random() * (99 - 1)) + 10;
		int r3 = (int) (Math.random() * (9999 - 1)) + 1000;
		sSSN = Integer.toString(r1) + "-" + Integer.toString(r2) + "-" + Integer.toString(r3);
		return sSSN;
	}

	public static String randomNameSuffix() {
		String sName = null;
		int r1 = (int) (Math.random() * 100);
		sName = Integer.toString(r1);
		return sName;
	}

	public static boolean VerifyOnPage(String sPageName) {
		boolean bFound = false;
		if (TestBase.getDriverInstance().getTitle().equals(sPageName)) {
			bFound = true;
		}
		return bFound;
	}

	// Hash table utilities
	public void printFieldValues(Hashtable<String, String> hash) throws Exception {
		Enumeration<String> names;
		names = hash.keys();
		while (names.hasMoreElements()) {
			String key = names.nextElement();
			System.out.println("Key: " + key + " & Value: " + hash.get(key));
		}
	}

	public static void saveFieldValues(Hashtable<String, String> hash, String sFilename) throws Exception {
		Enumeration<String> names;
		names = hash.keys();
		try (OutputStream out = new FileOutputStream(sFilename)) {
			Properties properties = new Properties();
			while (names.hasMoreElements()) {
				String key = names.nextElement();
				properties.setProperty(key, hash.get(key));
			}
			properties.store(out, "Hashtable Save");
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static Hashtable<String, String> loadFieldValues(String sFilename) throws Exception {
		Hashtable<String, String> hash = new Hashtable<String, String>();
		try (InputStream in = new FileInputStream(sFilename)) {

			Properties prop = new Properties();
			prop.load(in);
			// for each statement
			for (String property : prop.stringPropertyNames()) {
				String value = prop.getProperty(property);
				System.out.println(property + " = " + value);
				hash.put(property, value);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return hash;
	}

	public static boolean SQLQuery(String sDB_Server, String sDatabase, String sQuery)
			throws ClassNotFoundException, SQLException {
		// String query = "select LastName, FirstName from [User] where
		// IsLoanOfficer = 1 order by LastName, FirstName asc";
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerTestBase.getDriverInstance()");
		Connection connection = DriverManager.getConnection(
				"jdbc:sqlserver://" + sDB_Server + ";database=" + sDatabase + ";integratedSecurity=true;");
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery(sQuery);
		while (rs.next()) {
			String sRes1 = rs.getString("State");
			String sRes2 = rs.getString("County");
			System.out.println(sRes1 + ", " + sRes2);
		}
		return true;
	}

	public static ArrayList<String[]> ExecSQLQuery(String sDB_Server, String sDatabase, String sQuery)
			throws ClassNotFoundException, SQLException {

		ArrayList<String[]> result = new ArrayList<String[]>();
		ResultSet rs;
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		try {
			Connection connection = DriverManager.getConnection("jdbc:sqlserver://;servername=" + sDB_Server
					+ ";databaseName=" + sDatabase + ";integratedSecurity=true");
			Statement stmt = connection.createStatement();
			rs = stmt.executeQuery(sQuery);
		} catch (Exception e) {
			throw e;
		}

		int columnCount = rs.getMetaData().getColumnCount();
		while (rs.next()) {
			String[] row = new String[columnCount];
			for (int i = 0; i < columnCount; i++) {
				row[i] = rs.getString(i + 1);
			}
			result.add(row);
		}
		return result;
	}

	/**
	 * Run SQL Query
	 * 
	 * @param sDB_Server
	 *            DB Server
	 * @param sDatabase
	 *            Database Name
	 * @param sQuery
	 *            Query
	 * @return Result of Query
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static String[][] RunSQLQuery(String sDB_Server, String sDatabase, String sQuery)
			throws ClassNotFoundException, SQLException {
		ResultSet rs;
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		System.out.println(" DB Server -- " + sDB_Server + " Database -- " + sDatabase);
		System.out.println(" Running Query " + sQuery);
		try {
			Connection connection = DriverManager.getConnection("jdbc:sqlserver://;servername=" + sDB_Server
					+ ";databaseName=" + sDatabase + ";integratedSecurity=true");
			Statement stmt = connection.createStatement();
			rs = stmt.executeQuery(sQuery);
		} catch (Exception e) {
			throw e;
		}
		int iRes = rs.getFetchSize();
		ResultSetMetaData metadata = rs.getMetaData();
		int columnCount = metadata.getColumnCount();
		String[][] results = new String[iRes][columnCount];
		int iCnt = 0;
		while (rs.next()) {
			for (int i = 0; i < columnCount; i++) {
				results[iCnt][i] = rs.getString(i + 1);
			}
			iCnt++;
		}
		rs.close();
		System.out.println("Query execution completed...");
		return results;
	}

	public static String parseElement(String stobeParsed) {
		String sParsed = null;
		int iStart = stobeParsed.indexOf("^") + 1;
		sParsed = stobeParsed.substring(iStart);
		return sParsed;
	}

	public static String parseElementIdentifier(String stobeParsed) {
		String sParsed = null;
		int iEnd = stobeParsed.indexOf("^");
		sParsed = stobeParsed.substring(0, iEnd);
		return sParsed;
	}

	public static List<WebElement> getDropdownList(WebElement weElementID) {
		// ******* FORWARD DROP DOWN **********
		List<WebElement> allOptions;
		try {
			Select select = new Select(weElementID);
			allOptions = select.getOptions();
		} catch (Exception e) {
			// save error message to the log file
			Log.error(e.getMessage());
			throw (e);
		}
		return allOptions;
	}

	// WebElement option = select.getFirstSelectedOption();
	// option.getText
	public static String getSelectedItemfromDropdownList(WebElement weElementID) {
		// ******* FORWARD DROP DOWN **********
		WebElement option;
		try {
			Select select = new Select(weElementID);
			option = select.getFirstSelectedOption();
		} catch (Exception e) {
			// save error message to the log file
			Log.error(e.getMessage());
			throw (e);
		}
		return option.getText();
	}

	public static void SelectItemdropdownlist(WebElement weDD, String sListItem) {
		// ***** FORWARD DROP DOWN *****
		List<WebElement> allOptions = Utils.getDropdownList(weDD);
		java.util.Iterator<WebElement> i = allOptions.iterator();
		while (i.hasNext()) {
			WebElement row = i.next();
			System.out.println("Found: " + row.getAttribute("label"));
			if (row.getAttribute("label").equals(sListItem)) {
				row.click();
				// row.sendKeys(Keys.ENTER);
				break;
			}
		}
	}

	/**
	 * Selecting text from Lending portal
	 * 
	 * @param element
	 *            Web Element
	 * @param sElementText
	 *            Text to be selected
	 */
	public static void selectLendingDropDown(WebElement element, String sElementText) {
		System.out.println("Selecting text from dropdown as " + sElementText);
		Select dropElement = new Select(element);
		dropElement.selectByVisibleText(sElementText);
	}

	/*
	 * @Author :Akshay Sharma
	 * 
	 * @Application :Generic
	 * 
	 * @Description :To get range of dates between current date and the range
	 * provided
	 */
	public static List<String> getDateRange(int iRange) {
		String sCurrDate = "";
		List<String> lParsingDate = new ArrayList<>();
		int Size = iRange;
		int i = 0;

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/YYYY");
		Date date = new Date();
		sCurrDate = sdf.format(date);
		Calendar c = Calendar.getInstance();

		while (i < Size) {
			c.add(Calendar.DATE, 1); // number of days to add
			sCurrDate = sdf.format(c.getTime());
			lParsingDate.add(sCurrDate);
			i++;
		}

		for (String s : lParsingDate) {
			System.out.println(s);
		}
		return lParsingDate;
	}

	/*
	 * @Author :Akshay Sharma
	 * 
	 * @Application :Generic
	 * 
	 * @Description :To get Current System date in mm/dd/yyyy format
	 * 
	 */

	public static void setDropdownbyText(String sText) throws Exception {
		try {
			WebElement selectElement = TestBase.getDriverInstance()
					.findElement(By.xpath("//*[contains(text(), '" + sText + "')]"));
			selectElement.click();
		} catch (Exception e) {

			Log.error(e.getMessage());
			throw (e);
		}
	}

	public static void setRadDropdownItem(WebElement sElement_dropdown, int iIndex) throws Exception {
		// HELOS Reverse
		try {
			sElement_dropdown.click();
			waitFor(1);
			for (int i = 0; i < iIndex; i++) {
				sElement_dropdown.sendKeys(Keys.ARROW_DOWN);
				waitFor(1);
			}
			sElement_dropdown.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			Log.error(e.getMessage());
			throw (e);
		}
	}

	public static void SelectRadDropdownItembyIndex(WebElement weButton, WebElement weItem) throws Exception {
		// HELOS Reverse
		try {
			waitFor(2);
			weItem.click();
		} catch (Exception e) {
			Log.error(e.getMessage());
			// save error message to the log file
			try {

			} catch (Exception e2) {
				Log.error(e2.getMessage());
				throw e2;
			}
			throw e;
		}
	}

	public static ArrayList<String> getradDropdownItemList(WebElement weArrow, List<WebElement> welList)
			throws Exception {

		ArrayList<String> alFound = new ArrayList<String>();
		weArrow.click();
		Thread.sleep(4000);
		List<WebElement> classificationItems = welList;
		java.util.Iterator<WebElement> i = classificationItems.iterator();
		// get each dropdown item
		while (i.hasNext()) {
			WebElement row = i.next();
			System.out.println("Found: " + row.getText());
			alFound.add(row.getText());
		}
		weArrow.click();
		return alFound;
	}

	public static boolean checkforProcess(String sProcessName) throws IOException {
		Boolean bRes = false;
		int iLen = sProcessName.length() - 1;
		String line;
		Process p = Runtime.getRuntime().exec("tasklist.exe /fo csv /nh");
		BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
		while ((line = input.readLine()) != null) {
			if (line.substring(0, iLen).equals(sProcessName)) {
				bRes = true;
			}
		}
		input.close();
		return bRes;
	}

	public static boolean compareArrayLists(List<String> lUIDropdown, List<String> lDBDropdown) {
		if ((lUIDropdown.size() != lDBDropdown.size()) || (lUIDropdown == null && lDBDropdown != null)
				|| (lUIDropdown != null && lDBDropdown == null)) {
			return false;
		}
		// Sort and compare the two lists
		Collections.sort(lUIDropdown);
		Collections.sort(lDBDropdown);
		return lUIDropdown.equals(lDBDropdown);
	}

	public static void doubleClick(WebElement element) {
		try {
			Actions action = new Actions(TestBase.getDriverInstance()).doubleClick(element);
			action.build().perform();
			System.out.println("Double clicked the element");
		} catch (StaleElementReferenceException e) {
			System.out.println("Element is not attached to the page document " + e.getStackTrace());
		} catch (NoSuchElementException e) {
			System.out.println("Element " + element + " was not found in DOM " + e.getStackTrace());
		} catch (Exception e) {
			System.out.println("Element " + element + " was not clickable " + e.getStackTrace());
		}
	}

	/*
	 * @Author :Akshay Sharma
	 * 
	 * @Application :Generic
	 * 
	 * @Description :To get Current System date in mm/dd/yyyy format
	 * 
	 */

	public static boolean isDateEqual(String sDbDate, String sPageDate) {

		SimpleDateFormat DBdateFormat = new SimpleDateFormat("MM/DD/YYYY");
		SimpleDateFormat pagedateFormat = new SimpleDateFormat("MM/DD/YYYY");
		try {
			Date date1 = DBdateFormat.parse(sDbDate);
			Date date2 = pagedateFormat.parse(sPageDate);
			if (date1.compareTo(date2) == 0) {
				System.out.println(sDbDate + "=" + sPageDate + "Date Equal");
				return true;
			}
		} catch (Exception e) {
			Log.info("Date cannot be parsed ,Invalid Syntax");
		}

		return false;

	}

	/*
	 * @Author :Akshay Sharma
	 * 
	 * @Application :Generic
	 * 
	 * @Description :To get Current System date in mm/dd/yyyy format
	 * 
	 */
	public static String getCurrDate() {
		String CurrDate = "";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/YYYY");
		Date date = new Date();
		CurrDate = sdf.format(date);

		return CurrDate;
	}

	/*
	 * @Author :Akshay Sharma
	 * 
	 * @Application :Generic
	 * 
	 * @Description :To get Current System date in m/dd/yyyy format
	 * 
	 */
	public static String getCurrDateInMDDYYYY() {
		String CurrDate = "";

		SimpleDateFormat sdf = new SimpleDateFormat("M/dd/YYYY");
		Date date = new Date();
		CurrDate = sdf.format(date);
		System.out.println(CurrDate);
		return CurrDate;
	}

	/*
	 * @Author :Akshay Sharma
	 * 
	 * @Application :Generic
	 * 
	 * @Description : To get Current System date in mm/dd/yyyy hh:mm format
	 */
	public static String getCurrDateWithTime() {
		String sCurrDate = "";

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/YYYY hh:mm a");
		Date date = new Date();
		sCurrDate = sdf.format(date);

		return sCurrDate;
	}

	/**
	 * Scroll on element
	 * @param element Web Element
	 */
	public static void scroll_element_into_view(WebElement element) {
		System.out.println("Scrolling on element " + element);
		int Axis = (element.getLocation().getY() - 100);
		JavascriptExecutor js = (JavascriptExecutor) TestBase.getDriverInstance();
		js.executeScript("javascript:window.scrollTo(0," + Axis + ");");
	}

	/*
	 * @Author :Akshay Sharma
	 * 
	 * @Application :Generic
	 * 
	 * @Description :To Upload File
	 */
	public static void uploadPricingFile(String sFilePath) throws InterruptedException, AWTException {

		StringSelection path = new StringSelection(sFilePath);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(path, null);

		Thread.sleep(2000);// Sleep time to detect the window dialog box

		// Pasting the path in the File name field
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);

		// To click the Open button so as to upload file
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		try {
			waitFor(3);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * @Author :Akshay Sharma
	 * 
	 * @Application :Generic
	 * 
	 * @Description :To check if the field is blank or not
	 * 
	 * @Modified By :Sachin Raina
	 * 
	 * @ Description :Changed the element.gettext() to element.getAttribute("value")
	 */
	public static int isFieldEmpty(WebElement element) {
		String sFieldValue = "";
		sFieldValue = element.getAttribute("value");

		if (sFieldValue.isEmpty()) {
			return 1;

		} else {
			return 0;
		}
	}

	/**
	 * @description The method to select the element based on div tag.
	 * @author bhargavs
	 * @param labelText
	 * @param lenderType
	 * @return WebElement
	 */
	public static WebElement enableOnClick(String... args) {
		waitFor(5);
		WebElement element = null;
		WebDriverWait wait = new WebDriverWait(TestBase.getDriverInstance(), TimeConstants.elementMinorLoadTime);
		if (args.length == 1) {
			String xpathFormatter1 = String.format("//label[text()='%s']/parent::div//input[1]", args[0]);
			WebElement elem1 = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter1));
			wait.until(ExpectedConditions.elementToBeClickable(elem1)).click();
			String xpathFormatter2 = String.format("//label[text()='%s']/parent::div//input[2]", args[0]);
			element = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter2));
		} else if (args.length == 2) {
			try {
				String xpathFormatter1 = String
						.format("//div[@id='coBorrowerPanel']//label[text()='%s']/parent::div//input[1]", args[0]);
				WebElement elem1 = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter1));
				wait.until(ExpectedConditions.elementToBeClickable(elem1)).click();
				String xpathFormatter2 = String
						.format("//div[@id='coBorrowerPanel']//label[text()='%s']/parent::div//input[2]", args[0]);
				element = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter2));
			} catch (WebDriverException driverException) {
				String xpathFormatter1 = String
						.format("//div[@class='col-md-6'][2]//label[text()='%s']/parent::div//input[1]", args[0]);
				WebElement elem1 = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter1));
				wait.until(ExpectedConditions.elementToBeClickable(elem1)).click();
				String xpathFormatter2 = String
						.format("//div[@class='col-md-6'][2]//label[text()='%s']/parent::div//input[2]", args[0]);
				element = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter2));
			}
		} else if (args.length == 3) {
			String xpathFormatter1 = String.format(
					"//h3[text()='%s']/parent::div/following-sibling::div//label[text()='%s']/parent::div/following-sibling::div//input[1]",
					args[0], args[1]);
			WebElement elem1 = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter1));
			wait.until(ExpectedConditions.elementToBeClickable(elem1)).click();
			String xpathFormatter2 = String.format(
					"//h3[text()='%s']/parent::div/following-sibling::div//label[text()='%s']/parent::div/following-sibling::div//input[2]",
					args[0], args[1]);
			element = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter2));
		}
		Assert.assertNotNull(element, "Unable to locate the element.");
		return element;
	}

	/**
	 * @description The method to select the element based on table(td) tag.
	 * @author bhargavs
	 * @param labelText
	 * @param lenderType
	 * @return WebElement
	 */
	public static WebElement enableOnClickForTable(String labelText, String lenderType) {
		waitFor(5);
		WebElement element;
		WebDriverWait wait = new WebDriverWait(TestBase.getDriverInstance(), TimeConstants.elementMinorLoadTime);
		if (lenderType != null && lenderType.equalsIgnoreCase(ApplicationConstants.lenderTypeCoBorrower)) {
			String xpathFormatter1 = String.format(
					"//div[@id='coBorrowerPanel']//label[text()='%s']/parent::td/following-sibling::td//input[1]",
					labelText);
			WebElement elem1 = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter1));
			scroll_element_into_view(elem1);
			wait.until(ExpectedConditions.elementToBeClickable(elem1)).click();
			String xpathFormatter2 = String.format(
					"//div[@id='coBorrowerPanel']//label[text()='%s']/parent::td/following-sibling::td//input[2]",
					labelText);
			element = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter2));
		} else {
			String xpathFormatter1 = String.format("//*[text()='%s']/parent::td/following-sibling::td//input[1]",
					labelText);
			System.out.println("xpathFormatter1" + xpathFormatter1);
			WebElement elem1 = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter1));
			scroll_element_into_view(elem1);
			wait.until(ExpectedConditions.elementToBeClickable(elem1));
			elem1.click();
			String xpathFormatter2 = String.format("//*[text()='%s']/parent::td/following-sibling::td//input[2]",
					labelText);
			System.out.println("xpathFormatter2" + xpathFormatter2);
			element = TestBase.getDriverInstance().findElement(By.xpath(xpathFormatter2));
		}
		Assert.assertNotNull(element, "Unable to locate the element.");
		return element;
	}

	/**
	 * @description Method to wait until the specified time for invisibility of an
	 *              element.
	 * @author bhargavs
	 * @param locatorname
	 * @param timeout
	 * @return
	 */
	public static boolean waitForInvisibilityOfElement(String locatorname, int timeout) {
		WebDriverWait wait = new WebDriverWait(TestBase.getDriverInstance(), timeout);
		Boolean isElementPresent = wait
				.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(locatorname)));
		return isElementPresent;
	}

	/**
	 * @author bhargavs
	 * @param input
	 *            - String
	 * @return boolean
	 * @description To check input string is not null.
	 */
	public static boolean isStringNotNull(String input) {
		boolean bStringNotNull = false;
		if (input != null && !input.isEmpty()) {
			bStringNotNull = true;
		}
		return bStringNotNull;
	}

	/**
	 * @author=rainasac Check whether field is empty and enter the respective data
	 */

	public static void enterValueIfEmpty(WebElement sElement, String sValue) {
		if (isFieldEmpty(sElement) == 1) {

			sElement.sendKeys(sValue);
			sElement.sendKeys(Keys.TAB);
			Log.info("Entered Value for:" + sElement);

		} else {
			sElement.sendKeys(Keys.TAB);

		}
	}

	/**
	 * @author sharmaks
	 * @param Drop
	 *            Down Value to be selected Select the item from the dropDown menu
	 *            Before calling this method First click on the drop down
	 * @throws Exception
	 */
	public static void SelectValueDrop(String sItemName) throws Exception {

		List<WebElement> options = TestBase.getDriverInstance().findElements(
				By.xpath("//div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner selectpicker']//li"));
		waitFor(2);
		for (WebElement option : options) {
			if (option.getText().equals(sItemName)) {
				option.click();
				break;
			}

		}
	}

	/**
	 * @author sharmaks
	 * @param WebElement
	 * @param String
	 *            Click Clears and enter value in the textbox
	 */
	public static void enterValueInTextBox(WebElement element, String sValue) {

		element.click();
		element.clear();
		element.sendKeys(sValue);

	}

	/**
	 * @author MathurNA
	 * @param Drop
	 *            Down Value to be selected Select the item from the dropDown menu
	 *            Before calling this method First click on the drop down
	 * @throws Exception
	 */
	public static WebElement selectDropdownValue(String sItemName) {
		String sXpath = "";
		sXpath = "//div[@class='dropdown-menu open']//*[text()='" + sItemName + "']";
		TestBase.getDriverInstance().findElement(By.xpath(sXpath)).click();
		waitFor(2);
		return element;
	}

	/**
	 * This method will return the company to be used based on the environment
	 * 
	 * @return
	 */
	public static String getCompany() {

		String envCompany = null;
		switch (Environment.valueOf(System.getProperty("environment"))) {
		case QA01:
			envCompany = ApplicationConstants.COMPANY_QA01;
			break;
		case PreProd:
			envCompany = ApplicationConstants.COMPANY_PREPROD01;
			break;
		case UAT01:
			envCompany = ApplicationConstants.COMPANY_UAT01;
			break;
		case QA02:
			envCompany = ApplicationConstants.COMPANY_QA02;
			break;

		default:
			break;
		}

		if (System.getProperty("application").equalsIgnoreCase("lendingportal")) {

			switch (Environment.valueOf(System.getProperty("environment"))) {
			case QA01:
				envCompany = "105 - " + envCompany;
				break;
			case PreProd:
				envCompany = "102 - " + envCompany;
				break;
			case UAT01:
				envCompany = "10008 - " + envCompany;
				break;
			case QA02:
				envCompany = "001 - " + envCompany;
				break;
			default:
				break;
			}
		}
		return envCompany;
	}

	/*
	 * This method will return the bank to be used based on the environment
	 * 
	 * @return
	 */
	public static String getBank() {

		String envBank = null;
		String exeEnv = System.getProperty("env");
		if (exeEnv.equalsIgnoreCase("qa01"))
			envBank = ApplicationConstants.BANK_QA01;
		if (exeEnv.equalsIgnoreCase("uat01"))
			envBank = ApplicationConstants.BANK_UAT01;
		if (exeEnv.equalsIgnoreCase("preprod01"))
			envBank = ApplicationConstants.BANK_PREPROD01;

		return envBank;
	}
	/*
	 * @author SachinRaina
	 * 
	 * @param Current URL (String) This methods gets the loan number from the URL
	 */

	public static String getLoanIdtFromUrl(String url) {
		return url.replaceFirst(".*/([^/?]+).*", "$1");
	}

	// save result to csv file
	public static void saveResult(String filename) {
		String COMMA_DELIMITER = ",";
		String NEW_LINE_SEPARATOR = "\n";

		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(filename);

			// set header

			fileWriter.append("Build");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append("Date");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append("Environment");
			fileWriter.append(NEW_LINE_SEPARATOR);
			// set system data
			fileWriter.append(TestBase.performance[0][0]);
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(TestBase.performance[0][1]);
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append(TestBase.performance[0][2]);
			fileWriter.append(NEW_LINE_SEPARATOR);

			fileWriter.append("Page");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append("Function");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append("Load Time");
			fileWriter.append(COMMA_DELIMITER);
			fileWriter.append("Result");
			fileWriter.append(NEW_LINE_SEPARATOR);

			for (int x = 0; x < TestBase.performance.length; x++) {
				if (TestBase.performance[x][0] != null) {
					System.out.println(x + ", " + TestBase.performance[x][0] + ", " + TestBase.performance[x][1] + ", "
							+ TestBase.performance[x][2]);
				}
			}

			for (int x = 1; x < TestBase.performance.length; x++) {
				if (TestBase.performance[x][0] != null) {
					fileWriter.append(TestBase.performance[x][0]);
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(TestBase.performance[x][1]);
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(TestBase.performance[x][2]);
					fileWriter.append(COMMA_DELIMITER);
					fileWriter.append(TestBase.performance[x][3]);
					fileWriter.append(NEW_LINE_SEPARATOR);
				}
			}
			System.out.println("CSV file was created successfully");
		} catch (Exception e) {
			System.out.println("Error in CSV file creation");
			e.printStackTrace();
		} finally {

			try {

				fileWriter.close();
			} catch (IOException e) {
				System.out.println("Error during CSV file closing");
				e.printStackTrace();
			}

		}

	}

	public static void ReportElapsedTime(long StartTime, long FinishTime, String sTimedItem, String sTimerReason,
			String sResult, int iItemNumber) {

		double totalTime = (FinishTime - StartTime);
		totalTime = totalTime / 1000;
		String sTotalTime = Double.toString(totalTime);
		System.out.println("Total Time for " + sTimerReason + " : " + sTimedItem + " " + sTotalTime + " sec");
		Log.info("Total Time for " + sTimerReason + " : " + sTimedItem + " " + sTotalTime + " sec");
		// setup array to hold data to report at the end of the script run
		int arrayindex = iItemNumber;
		int arrayLength = TestBase.performance.length;

		for (int i = 0; i < arrayLength; i++) {

			if (TestBase.performance[i][0] != null) {

				if (TestBase.performance[i][0].equals(sTimedItem)) {
					arrayindex = i;

					if ((sTimerReason.contains("SAVE")) || (sTimerReason.contains("save"))
							|| (sTimerReason.contains("Save"))) {

						System.out.println("Save load request ");
						arrayindex = i + 1;
					}
					break;
				}
			}
		}

		TestBase.performance[arrayindex][0] = sTimedItem;
		TestBase.performance[arrayindex][1] = sTimerReason;
		TestBase.performance[arrayindex][2] = sTotalTime;
		TestBase.performance[arrayindex][3] = sResult;
	}

	// get version
	public static String GetMAXVersion(String sPath) throws Exception {
		String sMAXVersion = null;

		try (BufferedReader br = new BufferedReader(new FileReader(sPath))) {
			String line;
			while ((line = br.readLine()) != null) {

				System.out.println("Release Version: " + line.substring(0, 15));
				if (line.substring(0, 16).equals("Release Version:")) {
					// parse to get version
					sMAXVersion = line.substring(22);
				}
			}
			br.close();
		} catch (Exception e) {
			throw e;
		}

		return sMAXVersion;
	}

	/*
	 * @author Akshay Sharma
	 *
	 */

	public static String getDataUsingJExecutor(String element, String selector) {

		String sValue = null;
		driver = TestBase.getDriverInstance();
		try {
			if (selector.equalsIgnoreCase("Id")) {
				sValue = (String) ((JavascriptExecutor) driver)
						.executeScript("return document.getElementById('" + element + "').value");
			} else if (selector.equalsIgnoreCase("xpath")) {
				sValue = (String) ((JavascriptExecutor) driver)
						.executeScript("return document.getElementByXPath('" + element + "').value");
			}

		} catch (WebDriverException driverException) {

		}
		return sValue;
	}

	/*
	 * @author Akshay sharma
	 * 
	 * @Description To get absoluteInteger Amount,removes special characters like
	 * $,. and removes last 3 characters from end $1,500.00 will be 1500
	 * 
	 * @parameters String amount
	 * 
	 * @returns Int
	 */
	public static int getIntAmount(String sAmount) {

		int iAbsoluteAmount = 0;
		String sAbsoluteAmount = "";
		sAmount = sAmount.replaceAll("[^\\d.]", "");
		int ilen = sAmount.length();
		sAbsoluteAmount = sAmount.substring(0, ilen - 3);
		iAbsoluteAmount = Integer.parseInt(sAbsoluteAmount);

		return iAbsoluteAmount;
	}

	/*
	 * @Author Akshay Sharma
	 * 
	 * @Description Select the day from the calendar which is sent as parameter
	 * 
	 * @click on calandar and store all the elements and while calling this method
	 * send it as a parameter
	 * 
	 * @param allDates,day
	 */

	public static void selectDate(List<WebElement> allDates, int day) {

		for (WebElement ele : allDates) {

			String sdate = ele.getText();

			if (sdate.equalsIgnoreCase(Integer.toString(day))) {
				ele.click();
				break;
			}

		}

	}

	public static String getDataFromFieldUsingJS(WebElement classTag, WebElement fieldTag) {

		JavascriptExecutor js = (JavascriptExecutor) TestBase.getDriverInstance();
		js.executeScript("arguments[0].click()", classTag);
		String value = (String) js.executeScript("return arguments[0].value", fieldTag);
		return value;
	}

	public static String getDataFromFieldUsingJS(WebElement fieldTag) {

		JavascriptExecutor js = (JavascriptExecutor) TestBase.getDriverInstance();
		String value = (String) js.executeScript("return arguments[0].value", fieldTag);
		return value;
	}

	public static void waitForSpinner() {
		String spinLoader = "div[class='fa fa-spin fa-spinner']";
		waitForInvisibilityOfElement(spinLoader, TimeConstants.pageMajorLoadTime);
		waitFor(2);
	}

	public static void createSmokeTestPerfFilepath() {
		String env = System.getProperty("environment");
		String app = System.getProperty("application");
		String channel = System.getProperty("channel");
		switch (app) {
		case "Forward":
			createFile(System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\MAX");
			createFile(System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\MAX\\" + env);
			createFile(System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\MAX\\" + env + "\\" + channel);
			break;
		case "AdminPortal":
			createFile(System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\AdminPortal");
			createFile(System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\AdminPortal\\" + env);

			break;
		case "LendingPortal":
			createFile(System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\LendingPortal");
			createFile(System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\LendingPortal\\" + env);
			createFile(System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\LendingPortal\\" + env + "\\" + channel);
			break;
		case "OrgAdminURL":
			createFile(System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\OrgAdminPortal");
			createFile(System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\OrgAdminPortal\\" + env);
			createFile(
					System.getProperty("user.dir") + "\\SmokeTestPerfFiles\\OrgAdminPortal\\" + env + "\\" + channel);
			break;
		case "Portal":

			break;

		default:
			break;
		}

	}

	public static void createFile(String pathName) {
		File file = new File(pathName);
		if (!file.exists()) {
			if (file.mkdir()) {
				System.out.println("Directory is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
	}
}
